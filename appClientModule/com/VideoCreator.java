package com;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.concurrent.TimeUnit;

import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.ICodec;

public class VideoCreator 
{
	private static final double FRAME_RATE = 25.0;
    private IMediaWriter movie;
	private String path;
    private PicturesLoader pl;
	
    private BufferedImage prev;
    
    public VideoCreator(String p,String width,String height,String speed,String m)
    {
    	path = p;    	
    	pl = new PicturesLoader(path,width,height);
    
    	createVideo(width,height,speed,m);
    }
    
    private void createVideo(String width,String height,String speed,String m)
    {
    	System.out.println("create video");
        int time = 0;
        int sp = Integer.valueOf(speed);
        int acc = sp;
        movie = ToolFactory.makeWriter(path +"/movie.mov");  
        movie.addVideoStream(0, 0, Integer.valueOf(width), Integer.valueOf(height));
        
        int streamIndex = 1;
        if(m != null)
        {
        	streamIndex = movie.addAudioStream(1, 0,  2, 44100);
        }
        
        int size = pl.paths.size();
        for (int i = 0; i < size; i++)
        { 
        	System.out.println(i +" of " + size);
        	System.gc();
        	acc = sp;
        	BufferedImage input = null;
        	while(acc > 0)
        	{
        		input = new BufferedImage(Integer.valueOf(width), Integer.valueOf(height), BufferedImage.TYPE_4BYTE_ABGR);
        		input.getGraphics().drawImage(pl.getFrame(i), 0,0,null);
        		/*if(prev != null)
        		{
        			int alpha = 255;
        			if(acc == 3)
        			{
        				alpha = 200;
        			}
        			else if(acc == 2)
        			{
        				alpha = 100;
        			}
        			else if(acc == 1)
        			{
        				alpha = 20;
        			}
        			decreaseAlpha(prev,alpha);
        			input.getGraphics().drawImage(prev, 0,0,null);
        		}*/
        		BufferedImage outPut = convertToType(input,BufferedImage.TYPE_3BYTE_BGR);
                time += (1000/FRAME_RATE);      

                movie.encodeVideo(0,outPut, time, TimeUnit.MILLISECONDS);  
        		acc = 0;
        	}
        	
    		prev = input;
        }
        
        if(m != null)
        {
        	Music music = new Music(m);                      
        	movie = music.addMusic(streamIndex, movie,time);
        }
        
        movie.close();
        System.out.println("Done");
    }
    
    public static void decreaseAlpha(BufferedImage img, int opacity)
    {
        int alfaDecrease = 255-opacity;

        for(int x=0; x<img.getWidth(); x++)
        {
            for(int y=0; y<img.getHeight(); y++)
            {
                int pixel = img.getRGB(x, y);
                Color col = new Color(pixel);
                int red = col.getRed();
                int green = col.getGreen();
                int blue = col.getBlue();
                int alpha = col.getAlpha() - alfaDecrease;
                
                if(alpha < 0) alpha = 0;
                else if(alpha > 255) alpha = 255;
                
                Color color = new Color(red,green,blue,alpha);
                
                img.setRGB(x, y, color.getRGB());
            }
        }
    }

    
    private static BufferedImage convertToType(BufferedImage sourceImage, int targetType) 
    {
           BufferedImage image;

           if (sourceImage.getType() == targetType) 
           {
               image = sourceImage;
           }
           else 
           {
               image = new BufferedImage(sourceImage.getWidth(), sourceImage.getHeight(), targetType);
               image.getGraphics().drawImage(sourceImage, 0, 0, null);
           }

           return image;
    }
}
