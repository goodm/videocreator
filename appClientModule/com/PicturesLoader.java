package com;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.imageio.ImageIO;

public class PicturesLoader 
{
    public ArrayList<String> paths;
    private BufferedImage pic;
    private int w;
    private int h;
    
    private int id;
    
    public PicturesLoader(String path, String width, String height)
    {
        paths = new ArrayList<String>();
            
        w = Integer.valueOf(width);
        h = Integer.valueOf(height);
        
        load(path);
    }
    
    public BufferedImage getFrame(int i)
    {        
    	if(id != i)
    	{
    		id = i;
	    	System.gc();
	        String f = paths.get(i);
	        pic = null;
	        try 
	        {      
	        	File file = new File(f);
	            pic = ImageIO.read(file);
	            int newWidth = new Double(w).intValue();
	            int newHeight = new Double(h).intValue();
	            
	            String time = new Date(file.lastModified()).toString();
	            
	            BufferedImage resized = new BufferedImage(newWidth, newHeight, pic.getType());
	            Graphics2D g = resized.createGraphics();
	
	            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	            g.drawImage(pic, 0, 0, newWidth, newHeight, 0, 0, pic.getWidth(), pic.getHeight(), null);
	            //g.drawString(time, 10, 20);
	            g.dispose();
	
	            pic = resized;
	        }
	        catch (IOException e) 
	        {
	            e.printStackTrace();
	        }
    	}
        
        return pic;
    }
    
    private void load(String path)
    {
        paths.clear();
        System.out.println("Load pics");
        File dir = new File(path);
	    String[] chld = dir.list();
	    
	    if(chld == null)
	    {
            throw new RuntimeException("There is no such a directory!");
	    }
	    else
	    {
            if(chld.length == 0)
            {
                throw new RuntimeException("No images in this directory!");
            }
	
            for(int i = 0; i < chld.length; i++)
	        {
                String fileName = chld[i];
	            String type = fileName.substring(fileName.length() - 3, fileName.length());
                if(type.compareToIgnoreCase("jpg") == 0 || type.compareToIgnoreCase("peg") == 0 || type.compareToIgnoreCase("png") == 0)
                {
                    paths.add(path+"/"+fileName);
                    //paths.add(path+"/"+fileName);
                }
	        }
	            
            Collections.sort(paths);
	    }
    }
}
