import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.PicturesLoader;
import com.VideoCreator;


public class Main extends JFrame implements ActionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel buttons;
    private JButton choose;
    private JTextField width;
    private JTextField height;
    private JTextField speed;
    private JFileChooser fc;
    private JFileChooser fm;
    
    private String musicPath = null;
    
    private JLabel musicLabel;
    
	public static void main(String[] args) 
	{
		new Main();
	}

	public Main() 
	{
		super();
        //add(new Board());

        buttons = new JPanel();
        getContentPane().add(buttons);
        buttons.setVisible(true);
        buttons.setLayout(null);
        buttons.setSize(400,400);
        buttons.setBackground(Color.BLACK);
        
        fc = new JFileChooser();
        fc.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY);
        fc.addActionListener(this);
        
        fm = new JFileChooser();
        fm.setFileSelectionMode( JFileChooser.FILES_ONLY);
        fm.addActionListener(this);
        
        choose = new JButton("Create video from pictures");
        choose.setBounds(20,20,300,30);
        choose.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent event) 
            {            	
            	fc.showOpenDialog(Main.this);
            }
        });
        buttons.add(choose);
        
        JLabel w = new JLabel("Width : ");
        w.setForeground(Color.white);
        w.setBounds(20,80,100,30);
        buttons.add(w);
        
        JLabel h = new JLabel("Height : ");
        h.setForeground(Color.white);
        h.setBounds(20,120,100,30);
        buttons.add(h);
        
        JLabel s = new JLabel("Speed : ");
        s.setForeground(Color.white);
        s.setBounds(20,160,100,30);
        buttons.add(s);
        
        width = new JTextField();
        width.setEditable(true);
        width.setText("800");
        width.setBounds(120,80,100,30);
        buttons.add(width);
        
        height = new JTextField();
        height.setEditable(true);
        height.setText("600");
        height.setBounds(120,120,100,30);
        buttons.add(height);
        
        speed = new JTextField();
        speed.setEditable(true);
        speed.setText("3");
        speed.setBounds(120,160,100,30);
        buttons.add(speed);
        
        JButton music = new JButton("Choose music file");
        music.setBounds(20,220,200,30);
        music.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent event) 
            {            	
            	fm.showOpenDialog(Main.this);
            }
        });
        buttons.add(music);
        
        JButton clear = new JButton("Clear music");
        clear.setBounds(220,220,100,30);
        clear.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent event) 
            {            	
            	musicPath = null;
            	musicLabel.setText("");
            }
        });
        buttons.add(clear);
        
        musicLabel = new JLabel("");
        musicLabel.setForeground(Color.white);
        musicLabel.setBounds(20,250,300,30);
        buttons.add(musicLabel);
        
        setSize(400,400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);        
        setResizable(false);        
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{        
		if(fc.getSelectedFile()!=null)
		{
		    String path = fc.getSelectedFile().getAbsolutePath();
		    if(path != null)
		    {
		        choose.setVisible(false);
		        ((JPanel)choose.getParent()).revalidate();
		        try 
		        {
					Thread.sleep(1);
				} 
		        catch (InterruptedException e1) 
		        {
					e1.printStackTrace();
				}
		        VideoCreator vc = new VideoCreator(path,width.getText(),height.getText(),speed.getText(),musicPath);
		    }
		}
		
		if(fm.getSelectedFile()!=null)
		{
			musicPath = fm.getSelectedFile().getAbsolutePath();
			musicLabel.setText(musicPath);
		}
	}

}